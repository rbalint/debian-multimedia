Task: Pure Data
Description: Packages for working with Pure Data
 Metapackage which installs packages, externals, libraries and plug-ins to work
 with Pure Data (aka Pd), a graphical realtime computer music and signal
 processing language.
Install: true

Depends: puredata | puredata-core

#Suggests: pd-extended
#Pkg-Description: Extended version of puredata with extra pd libraries, extensions and documentation.
# Pd-extended is an extended version of puredata, with some gui enhancements among other things.
# It is fully compatible with puredata extra libraries, such as pd-zexy,
# pd-plugin, pd-pan, etc. All though it replaces puredata, it can be installed and run next to it.
#License: BSD
#WNPP: 708476
#Comment: pd-extended is dead

Suggests: camomile
License: BSD-3
Homepage: https://github.com/pierreguillot/Camomile
Pkg-Description: dynamic plugin that allows to load and to control Pure Data patches inside a DAW
 Camomile is a meta-plugin, that allows to load Pd-patches as VST/VST3/LV2
 plugin into your favourite Digital Audio Workstation.

Depends: deken

Depends: pd-deken, pd-deken-apt

Depends: gem

Depends: pd-pdp, pd-3dp, pd-scaf

Depends: pd-ableton-link

Depends: pd-abstractions
Homepage: https://github.com/residuum/Puredata-abstractions
Pkg-Description: Some abstractions for Puredata
 Simple loopers, effects and helpers.
Comment: this is a too-generic package name

Suggests: pd-acid-core
Homepage: https://github.com/chr15m/pd-acid-core
Pkg-Description: 303-style acid instrument for Pure Data.

Suggests: pd-acre
Homepage: https://git.iem.at/pd/acre
Pkg-Description: Algorithmic Composition Realtime Environment in Pure Data.

Suggests: pd-adaptive
Homepage: http://pure-data.svn.sourceforge.net/viewvc/pure-data/trunk/externals/grh/adaptive/
Pkg-Description: library for adaptive systems and filters
# UNDECIDED: is this still maintained??

Suggests: pd-algobreaks-core
Homepage: https://github.com/chr15m/pd-algobreaks-core
Pkg-Description: Pure Data patches for making procedural breakbeats.

Depends: pd-ambix

Depends: pd-arraysize

Depends: pd-aubio

Depends: pd-autopreset

Depends: pd-bandlimited
Homepage: https://github.com/pcasaes/bandlimited
License: Apache
Pkg-Description: bandlimited (non-aliasing) waveform generators for Pure Data
 A computational expensive Pure Data (Pd) external that generates signal (saw,
 triangle, square, and variable duty cycle pulse wave) band limited to the
 sampling rate.
WNPP: 842398

Depends: pd-bassemu

Depends: pd-beatpipe

Suggests: pd-blockhead
Homepage: https://github.com/chr15m/blockhead
Pkg-Description: A collection of Pure Data abstractions styled after rjlib.

Depends: pd-boids

Depends: pd-bsaylor

Depends: pd-chaos

Suggests: pd-cicm
Homepage: https://github.com/CICM/CicmWrapper
Pkg-Description: A C/TK library that aims to facilitate the creation of objects for Pure Data

Depends: pd-comport

Depends: pd-container
Homepage: http://grh.mur.at/software/pdcontainer.html
Pkg-Description: This library was made for algorithmic composition
# UNDECIDED: is this still maintained??

Suggests: pd-cream
Homepage: https://github.com/CICM/CreamLibrary
Pkg-Description: A set of PD externals for those who like vanilla... but also want some chocolate, coffee or caramel.

Depends: pd-creb

Depends: pd-csound

Depends: pd-cxc

Depends: pd-cyclone

Depends: pd-earplug

Depends: pd-ehu
Homepage: https://github.com/enrike/ehu-abstractions
License: GPL-2
Pkg-Description: GUI objects for rapid prototyping of interactive systems.
 EHU is a set of abstractions for the Pure Data real-time graphical programming
 environment. 
 EHU aims to allow for rapid prototyping of interactive systems. 
 Most abstractions in EHU encapsulate the GUI widgets to control them minimising
 the job of creation of widgets and connections.
 EHU covers playing video files, controlling video cameras, displaying pictures,
 playing samples, audio effects, DMX controls, and some general utilities.

Depends: pd-ekext

Depends: pd-ext13

Depends: pd-extendedview

Depends: pd-fftease

Depends: pd-flext-dev

Depends: pd-flite

Depends: pd-freeverb

Depends: pd-ggee

Depends: pd-gil

Depends: pd-hcs

Depends: pd-hexloader

Depends: pd-hid

Suggests: pd-hoa
Homepage: https://github.com/CICM/HoaLibrary-PD
Pkg-Description: Higher-order ambisonics of Pure Data

Depends: pd-iem

Depends: pd-iemutils

Depends: pd-iemambi

Depends: pd-iemgui
Pkg-Description: graphical objects for Pure Data
 iemgui adds a number of graphical elements to Pure Data.
 Some are general purpose, like the possibility to display images within
 the patch or to grab mouse events in a specific region.
 Other objects mainly deal with GUI-elements for positioning objects in
 2D- and 3D-spaces.
Homepage: http://puredata.info/
License: GPL
WNPP: 603180

Depends: pd-iemguts

Depends: pd-iemlib

Depends: pd-iemmatrix

Depends: pd-iemnet

Depends: pd-iemtab

Depends: pd-jmmmp

Depends: pd-jsusfx

Suggests: pd-kalman
Homepage: https://github.com/jwmatthys/kalman-pd
Pkg-Description: Simple control rate Kalman filter for Pure Data

Depends: pd-kollabs

Depends: pd-libdir

Depends: pd-list-abs

Depends: pd-log

Depends: pd-lua

Depends: pd-lyonpotpourri

Depends: pd-mapping

Depends: pd-markex

Depends: pd-maxlib

Depends: pd-mediasettings

Suggests: pd-midi-guis
Homepage: https://github.com/chr15m/pd-midi-guis
Pkg-Description: Abstractions for doing midi-learn buttons and faders in Pure Data.

Depends: pd-mjlib

Depends: pd-moonlib

Depends: pd-motex

Depends: pd-mrpeach, pd-mrpeach-net, pd-cmos, pd-slip, pd-xbee

Depends: pd-msd
Homepage: http://www.nimon.org/soft.php
Pkg-Description: MSD stands for the mass spring damper library
 It is an extension of pmpd, which allows particular physical modeling in
 Pd and Max/MSP software.

Suggests: pd-netro
Homepage: https://github.com/chr15m/netro
Pkg-Description: A way to synchronise Pd patches on the same LAN.

Depends: pd-nusmuk

Depends: pd-osc

Depends: pd-oscbank
Homepage: https://github.com/richardeakin/pd-oscbank
Pkg-Description: oscillator bank for additive synthesis
 This is a plugin for PureData. It was written in order to synthesize
 sinusoidal models that consist of hundreds of partial sinewave
 components.

Depends: pd-pan

Depends: pd-pddp

Depends: pd-pdogg

Depends: pd-pdstring

Depends: pd-pduino

Depends: pd-plugin

Depends: pd-pmpd

Depends: pd-pool

Depends: pd-puremapping

Depends: pd-purepd

Depends: pd-purest-json

Depends: pd-py

Depends: pd-readanysf

Suggests: pd-rtcmix
Homepage: https://github.com/jwmatthys/rtcmix-in-pd
Pkg-Description: RTcmix music language embedded in PureData

Depends: pd-rtclib

Suggests: pd-s-abstractions
Homepage: https://github.com/chr15m/s-abstractions
Pkg-Description: A collection of abstractions for puredata

Depends: pd-sigpack

Depends: pd-smlib

Suggests: pd-sssad
Homepage: https://github.com/chr15m/sssad
Pkg-Description: sssad is Stupidsupersimplistic State Saving ADVANCED

Suggests: pd-timeline
Homepage: https://github.com/MetaluNet/PdTl
Pkg-Description: A timeline for Pure Data

#Suggests: pd-tof
#Suggests: pd-jasch
#Suggests: pd-moocow
#Suggests: pd-urlloader

Suggests: pd-search-plugin
Homepage: http://puredata.info/downloads/searchplugin
Pkg-Description: Interactive Search for Pd's documentation

Depends: pd-syslog

Depends: pd-tclpd

Depends: pd-testtools

Depends: pd-unauthorized

Depends: pd-upp

Suggests: pd-vasp
Homepage: https://github.com/grrrr/vasp
Pkg-Description: VASP modular - Vector assembling signal processor for PD
WNPP: 843044
Comment: pd-vasp has a a non-free dependency (shipped with the code)

Depends: pd-vbap

Depends: pd-wiimote

Depends: pd-windowing

Depends: pd-xsample

Depends: pd-zexy
